package com.atlassian.examples;

import com.atlassian.confluence.admin.criteria.MailServerExistsCriteria;
import com.atlassian.plugin.PluginParseException;
import com.atlassian.plugin.web.Condition;

import java.util.Map;

public class DefaultMailServerExistsCondition implements Condition
{
    final MailServerExistsCriteria mailServerExistsCriteria;

    public DefaultMailServerExistsCondition(MailServerExistsCriteria mailServerExistsCriteria)
    {
        this.mailServerExistsCriteria = mailServerExistsCriteria;
    }

    @Override public void init(Map<String, String> params) throws PluginParseException
    {
        // Do nothing
    }

    @Override public boolean shouldDisplay(Map<String, Object> context)
    {
        return mailServerExistsCriteria.isMet();
    }
}
